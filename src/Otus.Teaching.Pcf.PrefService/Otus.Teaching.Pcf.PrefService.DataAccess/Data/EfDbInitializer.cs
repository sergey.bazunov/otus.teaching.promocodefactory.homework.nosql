﻿using Otus.Teaching.Pcf.PrefService.DataAccess.Data;
using Otus.Teaching.Pcf.PrefService.DataAccess.DataContext;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly EFDataContext _dataContext;

        public EfDbInitializer(EFDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();            
        }
    }
}