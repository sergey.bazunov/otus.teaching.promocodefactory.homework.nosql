﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.PrefService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.PrefService.Core.Domain;
using Otus.Teaching.Pcf.PrefService.Core.Utils;
using Otus.Teaching.Pcf.PrefService.DataAccess.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.PrefService.DataAccess.Repositories
{
    public class EntityFrameworkRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly EFDataContext dataContext;
        private readonly IDistributedCache distributedCash;
        private readonly ILogger<Repositories.EntityFrameworkRepository<T>> logger;
        private readonly DistributedCacheEntryOptions options;


        public EntityFrameworkRepository(EFDataContext dataContext, IDistributedCache distributedCash, ILogger<EntityFrameworkRepository<T>> logger)
        {
            this.dataContext = dataContext;
            this.distributedCash = distributedCash;
            this.logger = logger;
            this.options = new DistributedCacheEntryOptions()
                .SetAbsoluteExpiration(DateTime.Now.AddSeconds(10));
        }

        public async Task AddAsync(T entity)
        {
            try
            {
                await dataContext.AddAsync(entity);
                await dataContext.SaveChangesAsync();
                // should I cash newbies? think no.
            }
            catch (Exception e)
            {
                logger.LogError("Cannot add new Preference.", entity, e);
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            var found = await dataContext.Set<T>().FindAsync(id);

            if (found != null)
            {
                try
                {
                    dataContext.Remove(found);
                    await dataContext.SaveChangesAsync();
                    
                    // if cashed, remove
                    await distributedCash.RemoveAsync(id.ToString());
                }
                catch
                {
                    logger.LogError("Error occured while deleting Preference with id: " + id);
                }
            }
            else
            {
                logger.LogWarning("Cannot find preference for OP delete with id:" + id);
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync(PageParams pageParams)
        {
            string keyPrefix = typeof(T).ToString();
            logger.LogInformation("Requiested type: " + keyPrefix);

            string dcKey = keyPrefix + ":" + pageParams.PageNumber.ToString() + ":" + pageParams.PageSize.ToString();
            byte[] bytes = await distributedCash.GetAsync(dcKey);

            if (bytes == null)
            {
                List<T> prefernces = null;
                try
                {
                    prefernces = await dataContext.Set<T>()
                    .Skip((pageParams.PageNumber - 1) * pageParams.PageSize)
                    .Take(pageParams.PageSize)
                    .OrderBy(p => p.Id)
                    .AsNoTracking()
                    .ToListAsync();
                }
                finally
                {
                    if (prefernces != null)
                        PleaseCash(dcKey, prefernces);
                }
                return prefernces;
            }
            else
            {
                List<T> prefs = JsonConvert.DeserializeObject<List<T>>(System.Text.Encoding.UTF8.GetString(bytes));
                return prefs;
            }
        }

        private async void PleaseCash(string dcKey, List<T> prefernces)
        {
            string json = JsonConvert.SerializeObject(prefernces, Formatting.None);
            await distributedCash.SetAsync(dcKey, System.Text.Encoding.UTF8.GetBytes(json), options);
        }

        public async Task<IEnumerable<T>> GetAllAsync(Func<T, bool> predicate)
        {
            var items = dataContext.Set<T>().Where(predicate);
            return await Task.FromResult(items);
        }

        public async Task<T> GetOneAsync(Func<T, bool> predicate)
        {
            var item = dataContext.Set<T>().Where(predicate).FirstOrDefault();
            return await Task.FromResult(item);
        }

        public async Task<T> GetOneAsync(Guid id)
        {
            var bytes = await distributedCash.GetAsync(id.ToString());
            if (bytes == null)
            {
                var pref = await dataContext.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
                
                if (pref == null)
                    return await Task.FromResult<T>(null);
                                
                var json = JsonConvert.SerializeObject(pref, Formatting.None);
                await distributedCash.SetAsync(pref.Id.ToString(), Encoding.UTF8.GetBytes(json), options);

                return await Task.FromResult<T>(pref);
            }
            else
            { 
                string json = Encoding.UTF8.GetString(bytes);
                T item = JsonConvert.DeserializeObject<T>(json);
                return item;
            }
        }

        public async Task<IEnumerable<T>> GetRangeAsync(ICollection<Guid> ids)
        {
            return await dataContext.Set<T>()
                .Where(x => ids.Contains(x.Id))
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            try
            {
                await dataContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.LogError("Cannot update/save changes in database. " + ex.Message);
            }
            await distributedCash.RemoveAsync(entity.Id.ToString());
        }
    }
}
