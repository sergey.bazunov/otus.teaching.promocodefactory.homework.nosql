﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.PrefService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.PrefService.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using System.Net;
using System.Linq;

namespace Otus.Teaching.Pcf.PrefService.DataAccess.Repositories
{
    /*
    public class RedisRepository<T> : IRepository<T> where T : Preference
    {
        private readonly IDistributedCache redis;
        private const string RedisKeyPrefix = "PrefService:Preferences";
        private readonly string RedisCacheConnectionstring = "";

        public RedisRepository(IDistributedCache distributedCash)
        {
            this.redis = distributedCash;
        }

        /// <summary>
        /// Store into redisdb Preference with key: PrefService:Preferences:{guid},
        /// where guid is Preference.Id
        /// </summary>
        /// <param name="entity">Preference object</param>
        /// <returns></returns>
        public Task AddAsync(T entity)
        {
            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(entity));
            return redis.SetAsync(RedisKeyPrefix + entity.Id, data);
        }

        public Task DeleteAsync(T entity)
        {
            return redis.RemoveAsync(RedisKeyPrefix + entity.Id);
        }

        /// <summary>
        /// Find all keys in Redis db
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var pattern = $"{RedisKeyPrefix}*";
            ConfigurationOptions options = ConfigurationOptions.Parse(RedisCacheConnectionstring);
            ConnectionMultiplexer connection = ConnectionMultiplexer.Connect(options);
            IDatabase db = connection.GetDatabase();
            EndPoint endPoint = connection.GetEndPoints().First();
            RedisKey[] keys = connection.GetServer(endPoint).Keys(pattern: pattern).ToArray();
            var server = connection.GetServer(endPoint);

            var result = new List<T>();
            foreach (var key in server.Keys())
            {
                byte[] data = await redis.GetAsync(key);
                result.Add(JsonConvert.DeserializeObject<T>(data.ToString()));
            }
            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            byte[] data = await redis.GetAsync(RedisKeyPrefix + id);
            if (data != null)
            {
                var pref = JsonConvert.DeserializeObject<T>(data.ToString());
                return pref;
            }
            return await Task.FromResult<T>(null);
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var prefs = await GetAllAsync();
            if (prefs == null) return await Task.FromResult<T>(null);

            var found = prefs.Where(predicate.Compile()).FirstOrDefault();
            if (found != null) return found;

            return await Task.FromResult<T>(null);
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            IEnumerable<T> prefs = await GetAllAsync();
            return prefs.Where(p => ids.Contains(p.Id));
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var prefs = await GetAllAsync();
            var items = prefs.Where(predicate.Compile());
            return items;
        }

        public Task UpdateAsync(T entity)
        {
            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(entity));
            return redis.SetAsync(RedisKeyPrefix + entity.Id, data);
        }
    }
    */
}
