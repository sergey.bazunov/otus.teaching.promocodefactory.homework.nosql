﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.PrefService.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.PrefService.DataAccess.DataContext
{
    public class EFDataContext : DbContext
    {  
        public DbSet<Preference> Preferences { get; set; }

        public EFDataContext()
        {
        }

        public EFDataContext(DbContextOptions<EFDataContext> options) : base(options)
        { 
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
