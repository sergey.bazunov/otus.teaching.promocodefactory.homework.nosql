﻿using Otus.Teaching.Pcf.PrefService.Core.Domain;
using Otus.Teaching.Pcf.PrefService.Core.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.PrefService.Core.Abstractions.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(PageParams pageParams);
        Task<IEnumerable<T>> GetAllAsync(Func<T, bool> predicate);

        Task<T> GetOneAsync(Guid id);
        Task<T> GetOneAsync(Func<T, bool> predicate);

        Task AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(Guid id);

        Task<IEnumerable<T>> GetRangeAsync(ICollection<Guid> ids);        
    }
}
