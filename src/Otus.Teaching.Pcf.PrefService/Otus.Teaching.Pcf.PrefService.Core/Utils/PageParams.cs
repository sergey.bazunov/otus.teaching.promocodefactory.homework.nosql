﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.PrefService.Core.Utils
{
    public class PageParams
    {
        private const int _maxPageSize = 50;

        private int _pageSize = 10;
        private int _pageNumber = 1;

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > _maxPageSize) ? _maxPageSize : value;
            }
        }

        public int PageNumber
        {
            get
            {
                return _pageNumber;
            }
            set
            {
                _pageNumber = (value <= 0) ? 1 : value;
            }
        }
    }
}
