﻿using System;

namespace Otus.Teaching.Pcf.PrefService.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}