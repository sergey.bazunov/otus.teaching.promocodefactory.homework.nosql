﻿namespace Otus.Teaching.Pcf.PrefService.Core.Domain
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}