using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Npgsql;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.PrefService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.PrefService.Core.Domain;
using Otus.Teaching.Pcf.PrefService.DataAccess.DataContext;
using Otus.Teaching.Pcf.PrefService.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.PrefService.WebHost
{
    public class Startup
    {
        // Redis
        private string redisHost = Environment.GetEnvironmentVariable("REDIS_HOST");
        private string redisPort = Environment.GetEnvironmentVariable("REDIS_PORT");

        // PosgresDB
        string postgresHost = Environment.GetEnvironmentVariable("POSTGRES_HOST");
        string postgresPort = Environment.GetEnvironmentVariable("POSTGRES_PORT");
        string postgresDbName = Environment.GetEnvironmentVariable("POSTGRES_DB");
        string postgresUser = Environment.GetEnvironmentVariable("POSTGRES_USER");
        string postgresPass = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");

        string postgresConnectionString = Environment.GetEnvironmentVariable("PromocodeFactoryPrefServiceDb");

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // dbcontext

            //string ptrn = $"Host={postgresHost};Port={postgresPort};Database={postgresDbName};Username={postgresUser};Password={postgresPass}";            
            //Console.WriteLine("[Connection string] " + ptrn);

            // dataaccess layer (repository)
            services.AddScoped(typeof(IRepository<>), typeof(EntityFrameworkRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddDbContext<EFDataContext>(x =>
            {
                //x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryPrefServiceDb"));
                x.UseNpgsql(postgresConnectionString);
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });


            // redis service
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = redisHost + ":" + redisPort;
            });

            // swagger service
            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Preferences API Doc";
                options.Version = "1.0";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer initializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            initializer.InitializeDb();
        }
    }
}
