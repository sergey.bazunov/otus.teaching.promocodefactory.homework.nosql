﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.PrefService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.PrefService.Core.Domain;
using Otus.Teaching.Pcf.PrefService.Core.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Otus.Teaching.Pcf.PrefService.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> repository;
        private readonly ILogger logger;

        public PreferencesController(IRepository<Preference> repository, ILogger<PreferencesController> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetPreferencesAsync([FromQuery] PageParams pageParams)
        {
            var items = await repository.GetAllAsync(pageParams);
            logger.LogInformation($"Request page {pageParams.PageNumber} with with size {pageParams.PageSize}");
            return Ok(items);
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Preference>> GetPreferenceAsync(Guid id)
        {
            var item = await repository.GetOneAsync(id);
            logger.LogInformation($"Request Preference with id: " + id.ToString());
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult> CreatePreference([FromBody] string PreferenceName)
        {
            if (string.IsNullOrEmpty(PreferenceName))
                return BadRequest();

            var item = await GenerateNew(PreferenceName);
            if (item == null) return BadRequest();
            
            await repository.AddAsync(item);
            
            logger.LogInformation($"Created new Preference with id: " + item.Id);

            return Ok(item);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdatePreference(Guid id, [FromBody] string PreferenceName)
        {
            if (string.IsNullOrEmpty(PreferenceName))
                return BadRequest();

            var item = await repository.GetOneAsync(id);
            if (item == null)
            {
                logger.LogWarning($"Preference with id {id} not found.");
                return NotFound();
            }

            item.Name = PreferenceName;

            await repository.UpdateAsync(item);
            
            logger.LogInformation($"Preference updated with id: " + item.Id);

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            await repository.DeleteAsync(id);
            
            logger.LogInformation(id + " deleted successfully.");
            return Ok();
        }

        /// <summary>
        /// BLL part
        /// </summary>
        /// <param name="PreferenceName">New Preference name</param>
        /// <returns>Valid preference obj</returns>
        private async Task<Preference> GenerateNew(string PreferenceName)
        {
            var item = await repository.GetOneAsync(p => p.Name.ToLower() == PreferenceName.ToLower());
            if (item == null)
            { 
                return await Task.FromResult(new Preference() { Id = Guid.NewGuid(), Name = PreferenceName });
            }

            logger.LogWarning($"Item with name {PreferenceName} already exists.");
            return await Task.FromResult<Preference>(null);
        }

    }
}
