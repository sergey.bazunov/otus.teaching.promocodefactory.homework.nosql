﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions
{
    public interface IPreferenceManagement
    {
        Task<Preference> GetByIdAsync(Guid id);
        Task<IEnumerable<Preference>> GetAllAsync();
    }
}
