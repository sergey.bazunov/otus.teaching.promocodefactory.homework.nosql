﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class PreferenceManagement : IPreferenceManagement
    {
        private readonly HttpClient httpClient;

        public PreferenceManagement(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<IEnumerable<Preference>> GetAllAsync()
        {
            using var httpResponse = await httpClient.GetAsync("api/Preferences}");

            httpResponse.EnsureSuccessStatusCode();

            var content = await httpResponse.Content.ReadAsStringAsync();
            List<Preference> prefs = JsonConvert.DeserializeObject<List<Preference>>(content);

            return prefs;
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            using var httpResponse = await httpClient.GetAsync($"api/Preferences/{id}");

            httpResponse.EnsureSuccessStatusCode();

            var contentStream = await httpResponse.Content.ReadAsStreamAsync();

            return await System.Text.Json.JsonSerializer.DeserializeAsync<Preference>(
                contentStream,
                new System.Text.Json.JsonSerializerOptions
                {
                    IgnoreNullValues = true,
                    PropertyNameCaseInsensitive = true
                });

        }
    }
}
