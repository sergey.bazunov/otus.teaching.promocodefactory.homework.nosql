﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions
{
    public interface IPreferenceManagement
    {
        Task<Preference> GetByIdAsync(Guid id);
    }
}
