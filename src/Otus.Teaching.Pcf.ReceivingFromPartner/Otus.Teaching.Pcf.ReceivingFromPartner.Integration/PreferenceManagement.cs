﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PreferenceManagement : IPreferenceManagement
    {
        private readonly HttpClient httpClient;

        public PreferenceManagement(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            // url example: http://localhost:8080/api/Preferences/ef7f299f-92d7-459f-896e-078ed53ef99c
            using var httpResponse = await httpClient.GetAsync($"api/Preferences/{id}");

            httpResponse.EnsureSuccessStatusCode();

            var contentStream = await httpResponse.Content.ReadAsStreamAsync();

            return await System.Text.Json.JsonSerializer.DeserializeAsync<Preference>(
                contentStream,
                new System.Text.Json.JsonSerializerOptions
                {
                    IgnoreNullValues = true,
                    PropertyNameCaseInsensitive = true
                });

        }
    }
}
