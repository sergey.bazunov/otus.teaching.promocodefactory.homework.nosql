﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IRepository<Role> rolesRepo;
        private readonly IRepository<Employee> employeeRepo;

        public MongoDbInitializer(IRepository<Role> rolesRepo, IRepository<Employee> employeeRepo )
        {
            this.rolesRepo = rolesRepo;
            this.employeeRepo = employeeRepo;
        }

        public void InitializeDb()
        {
            foreach (Role r in FakeDataFactory.Roles)
            {
                rolesRepo.AddAsync(r);
            }

            foreach (Employee e in FakeDataFactory.Employees)
            {
                employeeRepo.AddAsync(e);
            }
        }
    }
}
