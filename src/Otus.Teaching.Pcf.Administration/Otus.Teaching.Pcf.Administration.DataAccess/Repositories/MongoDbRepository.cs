﻿using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.DataAccess.MongoDb;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoDbRepository(MongoDbSettings config)
        {
            var MongoClient = new MongoClient(config.ConnectionString);
            var MongoDatabase = MongoClient.GetDatabase(config.DatabaseName);            
            _collection = MongoDatabase.GetCollection<T>(typeof(T).ToString());
        }
       
        public Task AddAsync(T entity)
        {            
            return _collection.InsertOneAsync(entity);
        }

        public Task DeleteAsync(T entity)
        {
            return _collection.DeleteOneAsync(r => r.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _collection.AsQueryable().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _collection.Find(e => e.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            using IAsyncCursor<T> cursor = await _collection.FindAsync(e => ids.Contains(e.Id));
            return await cursor.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            using IAsyncCursor<T> cursor = await _collection.FindAsync(predicate);
            return await cursor.ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var update = entity.ToBsonDocument();
            await _collection.FindOneAndUpdateAsync(e => e.Id == entity.Id, update);
        }
    }
}
