namespace Otus.Teaching.Pcf.Administration.DataAccess.MongoDb
{
    public class MongoDbSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}
