﻿using System;

namespace Otus.Teaching.Pcf.Administration.Core.Domain
{
    public partial class BaseEntity
    {
        public Guid Id { get; set; }
    }
}